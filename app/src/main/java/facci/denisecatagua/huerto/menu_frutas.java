package facci.denisecatagua.huerto;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;

import facci.denisecatagua.huerto.adaptadores.adaptador_menu_frutas;
import facci.denisecatagua.huerto.entidades.frutas;

public class menu_frutas extends AppCompatActivity {
    menu_frutas esta_actividad;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.admin_menu_frutas);
        ArrayList<frutas> lista = (ArrayList<frutas>) getIntent().getSerializableExtra("miListaFruta");
        String a = "!";
        this.setTitle("Menu Frutas");
        esta_actividad = this;
        adaptador_menu_frutas adapter = new adaptador_menu_frutas(getApplicationContext(), lista);
        ListView listView_ = findViewById(R.id.ls_view_fruta);
        listView_.setAdapter(adapter);

        listView_.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                frutas obj_fruta = (frutas) (listView_.getItemAtPosition(position));

                Intent intent = new Intent(esta_actividad, frutas_vista.class);
                intent.putExtra("obj_fruta", obj_fruta  );
                startActivity(intent);

            }
        });
    }
}