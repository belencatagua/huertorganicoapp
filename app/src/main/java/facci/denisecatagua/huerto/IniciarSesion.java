package facci.denisecatagua.huerto;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import java.util.Objects;

public class IniciarSesion extends AppCompatActivity {

    static IniciarSesion esta_actividad;
    private EditText email, password;
    //private ProgressBar progressBar;
    private FirebaseAuth firebaseAuth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        esta_actividad = this;

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_iniciar_sesion);

        esta_actividad.setTitle("");

        Button registerBtn = findViewById(R.id.btn_register);
        Button loginBtn = findViewById(R.id.btn_iniciar_sesion);

        email = findViewById(R.id.et_correo);
        password = findViewById(R.id.et_contrasena);
       // progressBar = findViewById(R.id.progressBar);
        firebaseAuth = FirebaseAuth.getInstance();

        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(IniciarSesion.this,RegistrarUsuario.class));
                finish();
            }
        });

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String tex_email = email.getText().toString();
                String tex_password = password.getText().toString();

                if (TextUtils.isEmpty(tex_email) || TextUtils.isEmpty(tex_password)){
                    Toast.makeText(IniciarSesion.this, "Bienvendio al manual de huerto", Toast.LENGTH_SHORT).show();
                }
                else {
                    login(tex_email,tex_password);
                }
            }
        });
    }

    private void login(String tex_email, String tex_password) {
        //progressBar.setVisibility(View.VISIBLE);
        firebaseAuth.signInWithEmailAndPassword(tex_email, tex_password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
               // progressBar.setVisibility(View.GONE);
                if(task.isSuccessful()){
                    Intent intent = new Intent(IniciarSesion.this, menu_tipo_planta.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();
                }
                else{
                    Toast.makeText(IniciarSesion.this, Objects.requireNonNull(task.getException()).getMessage(), Toast.LENGTH_SHORT).show();

                }
            }
        });
    }
}