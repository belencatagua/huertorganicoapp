package facci.denisecatagua.huerto;

import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

public class herramientas_huerto extends AppCompatActivity {
    static herramientas_huerto esta_actividad;
    TextView ls_txt_basicas, ls_txt_opcionales;
    Button btn_herramientas;

    FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        esta_actividad = this;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.herramientas_huerto);
        esta_actividad.setTitle("Herramientas");
        db = FirebaseFirestore.getInstance();
        ls_txt_basicas = findViewById(R.id.txt_basicas);
        ls_txt_opcionales = findViewById(R.id.txt_opcionales);
        btn_herramientas = findViewById(R.id.btn_herramientas_basicas);



        obtenerDatos();
    }
    private void obtenerDatos(){
        db.collection("herramientas").document("1").get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                if (documentSnapshot.exists()){
                    String uno = documentSnapshot.getString("uno");
                    String dos = documentSnapshot.getString("dos");
                    String tres = documentSnapshot.getString("tres");
                    String cuatro = documentSnapshot.getString("cuatro");
                    String cinco = documentSnapshot.getString("cinco");
                    String seis = documentSnapshot.getString("seis");
                    String siete = documentSnapshot.getString("siete");
                    String uno1 = documentSnapshot.getString("1");
                    String dos2 = documentSnapshot.getString("2");
                    String tres3 = documentSnapshot.getString("3");
                    String cuatro4 = documentSnapshot.getString("4");
                    String cinco5 = documentSnapshot.getString("5");
                    String seis6 = documentSnapshot.getString("6");
                    String siete7 = documentSnapshot.getString("7");
                    ls_txt_basicas.setText(uno+"\n"+dos+"\n"+tres+"\n"+cuatro+"\n"+cinco+
                            "\n"+seis+"\n"+siete);
                    ls_txt_opcionales.setText(uno1+"\n"+dos2+"\n"+tres3+"\n"+cuatro4+
                            "\n"+cinco5+"\n"+seis6+"\n"+siete7+"\n");
                }
            }
        });
    }
}
