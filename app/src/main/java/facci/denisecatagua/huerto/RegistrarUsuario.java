package facci.denisecatagua.huerto;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Objects;

public class RegistrarUsuario extends AppCompatActivity {

    static RegistrarUsuario esta_actividad;

    private EditText userName, emailAddress, password;
    private Spinner ubicacion_spinner;
    private Button registerBtn;
    private ProgressBar progressBar;

    private FirebaseAuth firebaseAuth;
    private DatabaseReference databaseReference;

    Button btn_iniciarSesion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrar_usuario);
        esta_actividad.setTitle("");

        btn_iniciarSesion=findViewById(R.id.btn_iniciar_sesion);

        btn_iniciarSesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(RegistrarUsuario.this,IniciarSesion.class));
                finish();
            }
        });

        firebaseAuth = FirebaseAuth.getInstance();

        userName = findViewById(R.id.et_nomUsuario);
        emailAddress = findViewById(R.id.et_correo);
        password = findViewById(R.id.et_contrasena);
        registerBtn = findViewById(R.id.btn_register);
        progressBar = findViewById(R.id.progressBar);

        ubicacion_spinner = findViewById(R.id.spinner_ubicacion);

        String[]Ubicacion={
                "Parroquia - Ciudad",
                "Parroquia Urbana - Los esteros",
                "Parroquia Urbana - Manta",
                "Parroquia Urbana - San Mateo",
                "Parroquia Urbana - Eloy Alfaro",
                "Parroquia Urbana - Tarqui",
                "Parroquia Rural - San Lorenzo",
                "Parroquia Rural - Santa Marianita"

        };
        ArrayAdapter<String> adapter=new ArrayAdapter<>(this, android.R.layout.simple_spinner_item,Ubicacion);
        ubicacion_spinner.setAdapter(adapter);


        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String user_name = userName.getText().toString();
                final String email = emailAddress.getText().toString();
                final String txt_password = password.getText().toString();

                String ubicacion = ubicacion_spinner.getSelectedItem().toString().trim();

                if(TextUtils.isEmpty(user_name)  || TextUtils.isEmpty(email)
                        || TextUtils.isEmpty((txt_password))){
                    Toast.makeText(RegistrarUsuario.this, "Llenar todos los campos", Toast.LENGTH_SHORT).show();
                }
                else {
                    register(user_name,ubicacion,email,txt_password);
                }
            }
        });

    }

    private void register(String user_name, String direction, String email, String txt_password) {
        progressBar.setVisibility(View.VISIBLE);
        firebaseAuth.createUserWithEmailAndPassword(email,txt_password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    // Sign in success, update UI with the signed-in user's information
                    //Log.d(TAG, "signInWithCustomToken:success");
                    FirebaseUser rUser = firebaseAuth.getCurrentUser();
                    assert rUser != null;
                    String userId = rUser.getUid();
                    databaseReference = FirebaseDatabase.getInstance().getReference("Users").child(userId);
                    HashMap<String,String> hashMap = new HashMap<>();
                    hashMap.put("userId", userId);
                    hashMap.put("username", user_name);
                    hashMap.put("ubicacion", direction);
                    hashMap.put("email", email);
                    hashMap.put("password", txt_password);
                    databaseReference.setValue(hashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            progressBar.setVisibility(View.GONE);
                            if (task.isSuccessful()){
                                Intent intent = new Intent( RegistrarUsuario.this, facci.denisecatagua.huerto.IniciarSesion.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK| Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                            else {
                                Toast.makeText(RegistrarUsuario.this, Objects.requireNonNull(task.getException()).getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

                } else {
                    progressBar.setVisibility(View.GONE);
                    // If sign in fails, display a message to the user.
                    //Log.w(TAG, "signInWithCustomToken:failure", task.getException());
                    Toast.makeText(RegistrarUsuario.this, "Error al registar, compruebe datos",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
