package facci.denisecatagua.huerto;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;

import facci.denisecatagua.huerto.adaptadores.adaptador_medicinal;
import facci.denisecatagua.huerto.entidades.medicinales;

public class menu_medicinales extends AppCompatActivity {
    menu_medicinales esta_actividad2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.admin_menu_medicinales);
        ArrayList<medicinales> lista = (ArrayList<medicinales>) getIntent().getSerializableExtra("miLista");
        String a = "!";
        this.setTitle("Menu Medicinales");
        esta_actividad2 = this;

        adaptador_medicinal adapter = new adaptador_medicinal(getApplicationContext(), lista);
        ListView listView_ = findViewById(R.id.ls_view_medicinal);
        listView_.setAdapter(adapter);

        listView_.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                medicinales obj_medicinal = (medicinales) (listView_.getItemAtPosition(position));

                Intent intent = new Intent(esta_actividad2, medicinales_vista.class);
                intent.putExtra("obj_medicinal", obj_medicinal);
                startActivity(intent);

            }
        });
    }
}
