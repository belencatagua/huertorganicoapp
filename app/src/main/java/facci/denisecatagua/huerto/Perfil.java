package facci.denisecatagua.huerto;

import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class Perfil extends AppCompatActivity {

    static Perfil esta_actividad;

    private TextView userName, ubicacion, emailAddress, password;

    private FirebaseUser firebaseUser;
    private FirebaseAuth firebaseAuth;
    private DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        esta_actividad = this;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil);

        esta_actividad.setTitle("");

        /*style="@style/Base.ThemeOverlay.AppCompat.Dark.ActionBar"
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");*/

        userName = findViewById(R.id.et_nomUsuario);
        ubicacion = findViewById(R.id.et_ubicacion);
        emailAddress = findViewById(R.id.et_correo);
        password = findViewById(R.id.et_contrasena);

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseUser = firebaseAuth.getCurrentUser();

        databaseReference = FirebaseDatabase.getInstance().getReference("Users").child(firebaseUser.getUid());
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                UsersData usersData = snapshot.getValue(UsersData.class);
                assert usersData != null;
                userName.setText(usersData.getUsername());
                ubicacion.setText(usersData.getUbicacion());
                emailAddress.setText(usersData.getEmail());
                password.setText(usersData.getPassword());

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(Perfil.this, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });


    }
}