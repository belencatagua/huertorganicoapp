package facci.denisecatagua.huerto;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {

    static MainActivity esta_actividad;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        esta_actividad = this;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        esta_actividad.setTitle("");

        TimerTask tarea = new TimerTask(){
            @Override
            public void run(){
                Intent intent = new Intent(MainActivity.this, menu_presentacion.class);
                startActivity(intent);
                finish();
            }
        };
        Timer tiempo= new Timer();
        tiempo.schedule(tarea, 3400);
    }
}