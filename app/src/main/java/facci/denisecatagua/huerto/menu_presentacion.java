package facci.denisecatagua.huerto;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

import facci.denisecatagua.huerto.beneficios_huerto;
import facci.denisecatagua.huerto.entidades.medicinales;

public class menu_presentacion  extends AppCompatActivity {
    static menu_presentacion esta_actividad;
    Button btn_huerto, btn_general, btn_condiciones, btn_herramientas;
    TextView txt_descripcion;
    private FirebaseAnalytics mFirebaseAnalytics;
    ArrayList<beneficios_huerto> ls_beneficios = new ArrayList<beneficios_huerto>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        esta_actividad = this;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_presentacion);
        esta_actividad.setTitle("Menu Principal");
        setTheme(android.R.style.Theme);

        txt_descripcion = (TextView) findViewById(R.id.txt_descripcion);
       btn_general = (Button) findViewById(R.id.btn_inf_general);

        btn_general.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(esta_actividad, beneficios_huerto.class);
                startActivity(intent);
            }
        });
        btn_condiciones = (Button) findViewById(R.id.btn_condiciones_tomar);

        btn_condiciones.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(esta_actividad, condiciones_huerto.class);
                startActivity(intent);
            }
        });
        btn_herramientas = (Button) findViewById(R.id.btn_herramientas_basicas);

        btn_herramientas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(esta_actividad, herramientas_huerto.class);
                startActivity(intent);
            }
        });

        btn_huerto = (Button) findViewById(R.id.btn_huerta);
        btn_huerto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(esta_actividad, IniciarSesion.class);
                startActivity(intent);
            }
        });

    }
}
