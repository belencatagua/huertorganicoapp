
package facci.denisecatagua.huerto.entidades;

import java.io.Serializable;

public class medicinales implements Serializable {
    private String id;
    private String nombre;
    private String taxonomia;
    private String caracteristicas;
    private String requerimientos;
    private String ico;
    private String portada;

    public medicinales(){}

    public medicinales(String id_, String nombre_, String taxonomia_, String caracteristicas_, String requerimientos_, String ico_, String portada_ ){
        this.id = id_;
        this.nombre = nombre_;
        this.taxonomia = taxonomia_;
        this.caracteristicas = caracteristicas_;
        this.requerimientos = requerimientos_;
        this.ico = ico_;
        this.portada = portada_;
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getnombre() {
        return nombre;
    }

    public void setnombre(String nombre) {
        this.nombre = nombre;
    }

    public String gettaxonomia() {
        return taxonomia;
    }

    public void settaxonomia(String taxonomia) {
        this.taxonomia = taxonomia;
    }

    public String getcaracteristicas() {
        return caracteristicas;
    }

    public void setcaracteristicas(String caracteristicas) {
        this.caracteristicas = caracteristicas;
    }

    public String getrequerimientos() {
        return requerimientos;
    }

    public void setrequerimientos(String requerimientos) {
        this.requerimientos = requerimientos;
    }

    public String getico() {
        return ico;
    }

    public void setico(String ico   ) {
        this.ico = ico;
    }

    public String getportada() {
        return portada;
    }

    public void setportada(String portada   ) {
        this.portada = portada;
    }
    //dividido
}