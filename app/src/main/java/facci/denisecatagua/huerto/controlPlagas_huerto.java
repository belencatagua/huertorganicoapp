package facci.denisecatagua.huerto;

import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

public class controlPlagas_huerto extends AppCompatActivity {
    static controlPlagas_huerto esta_actividad;
    TextView ls_txt_descripcion;
    Button btn_general;

    FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        esta_actividad = this;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.control_plagas_huerto);
        esta_actividad.setTitle("Plaga");
        db = FirebaseFirestore.getInstance();
        ls_txt_descripcion = findViewById(R.id.txt_descripcion);
        btn_general = findViewById(R.id.btn_inf_general);



        obtenerDatos();
    }
    private void obtenerDatos(){
        db.collection("control").document("2").get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                if (documentSnapshot.exists()){
                    String descripcion = documentSnapshot.getString("descripcion");
                    String uno = documentSnapshot.getString("uno");
                    String dos = documentSnapshot.getString("dos");
                    String tres = documentSnapshot.getString("tres");
                    String cuatro = documentSnapshot.getString("cuatro");
                    ls_txt_descripcion.setText(descripcion+"\n"+"\n"+uno+"\n"+dos+
                            "\n"+tres+"\n"+cuatro+"\n");
                }
            }
        });
    }
}
