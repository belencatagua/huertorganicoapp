package facci.denisecatagua.huerto.adaptadores;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;

import facci.denisecatagua.huerto.R;
import facci.denisecatagua.huerto.entidades.frutas;

public class adaptador_menu_frutas extends ArrayAdapter<frutas> {
    private ArrayList<frutas> ls_frut_;
    ImageView imagen;
    Bitmap bitmapIMG;
    public adaptador_menu_frutas(Context context, ArrayList<frutas> ls_) {
        super(context,0, ls_);
        ls_frut_ = ls_;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        frutas menu = getItem(position);
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.adaptador_menu_frutas, parent,false);
        }
        TextView titulo_menu = (TextView) convertView.findViewById(R.id.titulo_menu);
        imagen = (ImageView) convertView.findViewById(R.id.img_fruta);

        titulo_menu.setText(menu.getnombre());
        new getImgUrl(imagen).execute(menu.getico());
        //new DownloadImageFromInternet(imagen).execute(menu.getico());
        //new DescargarImage(imagen).execute(menu.getico());
        //Bitmap imagen_ = getBitmapFromURL(menu.getico());
        //imagen.setImageBitmap(bitmapIMG);


        return convertView;
    }
    public class getImgUrl extends AsyncTask<String, Void, Bitmap>{
        ImageView imgView;

        public getImgUrl(ImageView imgView) {
            this.imgView = imgView;
        }

        @Override
        protected Bitmap doInBackground(String... url) {
            String urldisplay = url[0];
            bitmapIMG = null;
            try {
                InputStream srt = new URL(urldisplay).openStream();
                bitmapIMG = BitmapFactory.decodeStream(srt);
            } catch (Exception e) {
                Log.e("Error Message", e.getMessage());
                e.printStackTrace();
            }
            return  bitmapIMG;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {

            imgView.setImageBitmap(bitmap);
        }
    }


}


