package facci.denisecatagua.huerto;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.io.InputStream;

import facci.denisecatagua.huerto.entidades.medicinales;

public class medicinales_vista extends AppCompatActivity{
    TextView txt_taxonomia, txt_requerimientos, txt_titulo, txt_caracteristicas;
    ImageView img_portada;
    Bitmap bitmapIMG;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medicinales_vista);
        medicinales obj_seleccionado = (medicinales) getIntent().getSerializableExtra("obj_medicinal");
        this.setTitle("Medicinal "+ obj_seleccionado.getnombre());
        txt_taxonomia = findViewById(R.id.ht_txt_taxonomia);
        txt_requerimientos = findViewById(R.id.ht_txt_requerimientos);
        txt_titulo = findViewById(R.id.ht_lbl_titulo);
        txt_caracteristicas = findViewById(R.id.ht_txt_caracteristica);
        img_portada = findViewById(R.id.img_portada);
        String a = "a";
        new getImgUrl(img_portada).execute(obj_seleccionado.getportada());

        String s = "line 1" + System.getProperty ("line.separator") + "Line 2" + System.getProperty ("line.separator");
        txt_titulo.setText(obj_seleccionado.getnombre());
        //txt_taxonomia.setText(obj_seleccionado.gettaxonomia());
        //txt_caracteristicas.setText(obj_seleccionado.getcaracteristicas());
        //txt_requerimientos.setText(obj_seleccionado.getrequerimientos());

        String[] tax = obj_seleccionado.gettaxonomia().split("-");
        String s_tax = "";
        for(int l=0; l<tax.length; l++){
            s_tax = s_tax + tax[l].toString().trim() + System.getProperty ("line.separator");
        }
        txt_taxonomia.setText(s_tax);

        String[] carct = obj_seleccionado.getcaracteristicas().split("-");
        String s_carct = "";
        for(int l=0; l<carct.length; l++){
            s_carct = s_carct + carct[l].toString().trim() + System.getProperty ("line.separator");
        }
        txt_caracteristicas.setText(s_carct);

        String[] req = obj_seleccionado.getrequerimientos().split("-");
        String s_req = "";
        for(int l=0; l<req.length; l++){
            if (!req[l].toString().trim().isEmpty()){
                s_req = s_req + req[l].toString().trim() + System.getProperty ("line.separator");
                s_req =s_req +"" + System.getProperty ("line.separator");;
            }

        }
        txt_requerimientos.setText(s_req);
    }

    public class getImgUrl extends AsyncTask<String, Void, Bitmap> {
        ImageView imgView;

        public getImgUrl(ImageView imgView) {
            this.imgView = imgView;
        }

        @Override
        protected Bitmap doInBackground(String... url) {
            String urldisplay = url[0];
            bitmapIMG = null;
            try {
                InputStream srt = new java.net.URL(urldisplay).openStream();
                bitmapIMG = BitmapFactory.decodeStream(srt);
            } catch (Exception e) {
                Log.e("Error Message", e.getMessage());
                e.printStackTrace();
            }
            return  bitmapIMG;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {

            imgView.setImageBitmap(bitmap);
        }
    }



}