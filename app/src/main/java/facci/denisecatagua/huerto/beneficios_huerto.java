package facci.denisecatagua.huerto;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

public class beneficios_huerto extends AppCompatActivity {
    static beneficios_huerto esta_actividad;
    TextView ls_txt_descripcion;
    Button btn_general;

    FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        esta_actividad = this;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.beneficios_huerto);
        esta_actividad.setTitle("Beneficios");
        db = FirebaseFirestore.getInstance();
        ls_txt_descripcion = findViewById(R.id.txt_descripcion);
        btn_general = findViewById(R.id.btn_inf_general);



        obtenerDatos();
    }
    private void obtenerDatos(){
        db.collection("beneficios").document("1").get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                if (documentSnapshot.exists()){
                    String descripcion = documentSnapshot.getString("descripcion");
                    String uno = documentSnapshot.getString("uno");
                    String dos = documentSnapshot.getString("dos");
                    String tres = documentSnapshot.getString("tres");
                    String cuatro = documentSnapshot.getString("cuatro");
                    String cinco = documentSnapshot.getString("cinco");
                    String seis = documentSnapshot.getString("seis");
                    String siete = documentSnapshot.getString("siete");
                    String ocho = documentSnapshot.getString("ocho");
                    String nueve = documentSnapshot.getString("nueve");
                    String diez = documentSnapshot.getString("diez");
                    ls_txt_descripcion.setText(descripcion+"\n"+"\n"+"1.- "+uno+"\n"+"2.- "+dos+
                            "\n"+"3.- "+tres+"\n"+"4.- "+cuatro+"\n"+"5.- "+cinco+
                            "\n"+"6.- "+seis+"\n"+"7.- "+siete+"\n"+"8.- "+ocho+
                            "\n"+"9.- "+nueve+"\n"+"10.- "+diez+"\n");
                }
            }
        });
    }
}
