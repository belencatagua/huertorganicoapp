package facci.denisecatagua.huerto;

public class UsersData {
    private String userId;
    private String username;
    private String ubicacion;
    private String email;
    private String password;

    public UsersData(String userId, String username, String ubicacion, String email, String password) {
        this.userId = userId;
        this.username = username;
        this.ubicacion = ubicacion;
        this.email = email;
        this.password = password;
    }

    public UsersData() {
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
