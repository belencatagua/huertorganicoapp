package facci.denisecatagua.huerto;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import facci.denisecatagua.huerto.adaptadores.adaptador_menu;
import facci.denisecatagua.huerto.entidades.hortalizas;

public class menu_hortalizas extends AppCompatActivity {
    menu_hortalizas esta_actividad;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.admin_menu_hortalizas);
        ArrayList<hortalizas> lista = (ArrayList<hortalizas>) getIntent().getSerializableExtra("miLista");
        String a = "!";
        this.setTitle("Menu Hortalizas");
        esta_actividad = this;
        adaptador_menu adapter = new adaptador_menu(getApplicationContext(), lista);
        ListView listView_ = findViewById(R.id.ls_view_hortaliza);
        listView_.setAdapter(adapter);

        listView_.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                hortalizas obj_hortaliza = (hortalizas)(listView_.getItemAtPosition(position));

                Intent intent = new Intent(esta_actividad, hortalizas_vista.class);
                intent.putExtra("obj_hortaliza", obj_hortaliza  );
                startActivity(intent);

            }
        });
    }
}