package facci.denisecatagua.huerto;

import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

public class condiciones_huerto extends AppCompatActivity {
    static condiciones_huerto esta_actividad;
    TextView ls_txt_descripcion, ls_txt_lugar, ls_txt_suelo, ls_txt_agua, ls_txt_proteccion;
    Button btn_condiciones;

    FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        esta_actividad = this;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.condiciones_huerto);
        esta_actividad.setTitle("Condiciones");
        db = FirebaseFirestore.getInstance();
        ls_txt_descripcion = findViewById(R.id.txt_descripcion);
        ls_txt_lugar = findViewById(R.id.txt_lugar);
        ls_txt_suelo = findViewById(R.id.txt_suelo);
        ls_txt_agua = findViewById(R.id.txt_agua);
        ls_txt_proteccion = findViewById(R.id.txt_proteccion);
        btn_condiciones = findViewById(R.id.btn_condiciones_tomar);



        obtenerDatos();
    }
    private void obtenerDatos(){
        db.collection("condiciones").document("1").get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                if (documentSnapshot.exists()){
                    String descripcion = documentSnapshot.getString("descripcion");
                    String uno = documentSnapshot.getString("uno");
                    String dos = documentSnapshot.getString("dos");
                    String tres = documentSnapshot.getString("tres");
                    String cuatro = documentSnapshot.getString("cuatro");
                    ls_txt_descripcion.setText(descripcion+"\n");
                    ls_txt_lugar.setText(uno);
                    ls_txt_suelo.setText(dos);
                    ls_txt_agua.setText(tres);
                    ls_txt_proteccion.setText(cuatro+"\n");
                }
            }
        });
    }
}